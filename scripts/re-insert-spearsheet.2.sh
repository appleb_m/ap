#!/bin/bash

for i in {0001..5000}
do
  for l in light dark
  do
    f=output/run00${i}.index.${l}.out
    if [ -e ${f} ]
    then
      grep -v docs.google ${f} | egrep " google|Cannot find|gspread.exceptions.APIError" > /dev/null
      a=$?
      if [ $a == 0 ] 
      then
        b=`echo $f | sed 's/\.out//'`
        grep python $b | grep credentials > a
        if [ -s a ]
        then
            chmod +x a
            ./a > $f
        fi
        rm -rf a
      fi
    fi
  done
done

