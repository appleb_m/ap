#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Find mean diffracting resolution
#
# Copyright © 2014-2017 Deutsches Elektronen-Synchrotron DESY,
#                       a research centre of the Helmholtz Association.
#
# Author:
#    2014-2017 Thomas White <taw@physics.org>
#    2018      D. Ozerov (protected against zero indexed patterns, for sf-daq_ap)
#

import sys
import numpy
import matplotlib.pyplot as plt

if sys.argv[1] == '-':
    f = sys.stdin
else:
    f = open(sys.argv[1])

a = []

while True:
    fline = f.readline()
    if not fline:
        break
    if fline.find("diffraction_resolution_limit") != -1:
        res = float(fline.split('= ')[1].split(' ')[0].rstrip("\r\n"))
        a.append(res)
        continue

f.close()

if len(a) > 0:
    b = numpy.array(a)
    print(" Mean: {:.2} nm^-1 = {:.4} A".format(numpy.mean(b),10.0/numpy.mean(b)))
    print(" Best: {:.2} nm^-1 = {:.4} A".format(numpy.max(b),10.0/numpy.max(b)))
    print("Worst: {:.2} nm^-1 = {:.4} A".format(numpy.min(b),10.0/numpy.min(b)))
    print("Std deviation: {:.2} nm^-1".format(numpy.std(b)))
else:
    print(" Mean: {:.2} nm^-1 = nan A")
    print(" Best: {:.2} nm^-1 = nan A")
    print("Worst: {:.2} nm^-1 = nan A")
    print("Std deviation: nan")

