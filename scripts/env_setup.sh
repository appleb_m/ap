#!/bin/bash

export PGROUP=<EDIT HERE>

export BEAMLINE=<EDIT HERE>
#  Choice:      alvra bernina cristallina furka maloja

export DETECTOR_NAME=<EDIT HERE>
# Possible choice: JF06T08V04 JF06T32V04 JF17T16V01

export LOGBOOK=<EDIT HERE>
# Possible choice: None (if no google logbook feeling)

#-----------------------------------------------------

export GEOM_FILE=${DETECTOR_NAME}.geom

export BASEDIR=/sf/${BEAMLINE}/data/${PGROUP}/res

# Location of preinstalled conda environment. In case of different location - change next lines accordingly
source /sf/jungfrau/applications/miniconda3/etc/profile.d/conda.sh
conda activate ap

